﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

    [Header("Track generator")]
    public TrackGenerator trackGenerator;   //the track generator reference

    [Header("Player")]
    public GameObject playerPrefab; //Prefab for player
    public float flapPower; //the flapping strength

    [Header("UI")]
    public Text scoreText;  //text to displayer player scores
    public Text deathsText; //text to display player deaths

    private GameObject player;  //reference to the player object

    private Vector2 playerStartPos = new Vector2(-6, 0);    //where player starts as vector 2
    private int score;      //score 
    private int deaths;     //number of deaths

    private void Start()
    {
        trackGenerator.Init();     //Init the track generator

        player = Instantiate(playerPrefab, transform);  //Instantiate the player    
        player.GetComponent<Player>().Init(this, flapPower);    //And initialize it
     
        RespawnPlayer();    //just to be sure player start from right positiona nd score is 0, run player respawn
    }

    /// <summary>
    /// Respawns player, ran on player death
    /// </summary>
    private void  RespawnPlayer()
    {
        score = 0;      //Set score to 0
        scoreText.text = score.ToString(); //And reset it to be displayed

        player.transform.position = playerStartPos; // set player to the starting position
        trackGenerator.ResetPipes();    //Tell track generator to reset
    }

    /// <summary>
    /// called from player when he dies
    /// </summary>
    public void OnPlayerDead()
    {
        deaths++;   //Increment deaths count
        deathsText.text = deaths.ToString();    //Display it
        RespawnPlayer();    //Respawn the player
    }

    /// <summary>
    /// Called form player when he scores
    /// </summary>
    public void OnPlayerScore()
    {
        score++; //Increment the score count
        scoreText.text = score.ToString(); //And display it
    }
}

