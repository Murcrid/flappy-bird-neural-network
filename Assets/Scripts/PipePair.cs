﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// The Pipes
/// </summary>
public class PipePair : MonoBehaviour {

    [Header("Set up")]
    public GameObject pipeTop;
    public GameObject pipeBot;

    internal float speed;               //The movement speed of this pipe pair     

    private bool isInitialized;
    private float minDistance = 2.5f;   //Minimum distance between pipes
    private float maxDistance = 5.5f;   //Maximum distance between pipes

    /// <summary>
    /// Initialization of pipe pair.
    /// </summary>
    /// <param name="distance"></param>
    /// <param name="speed"></param>
    public void Init(float distance, float speed)
    {
        isInitialized = true;

        PositionPipes(distance);
        this.speed = speed;
    }

    /// <summary>
    /// Sets the distance between the two pipes
    /// </summary>
    /// <param name="distance"></param>
    private void PositionPipes(float distance)
    {
        if (isInitialized)
        {
                                                //First we check that the distance is within the limit
            if (distance > maxDistance)         //if they are more than the upperlimit
                distance = maxDistance;         //we set it to be the limit
            else if (distance < minDistance)    //or if it is less than the lowerlimit
                distance = minDistance;         //We set it to be the lowerlimit

            pipeTop.transform.localPosition = new Vector3(0, Random.Range(distance, maxDistance), 0);                       //First we set the top pipe into a random position
            pipeBot.transform.localPosition = pipeTop.transform.localPosition - new Vector3(0, distance + maxDistance, 0);  //Then we set the bot pipe certain distance away from the top pipe
        }
    }

    private void Update()
    {
        if(isInitialized)
            transform.Translate(Vector3.left * Time.deltaTime * speed); //We constantly move the pipe pair to the left with the speed of "speed"
    }
}
