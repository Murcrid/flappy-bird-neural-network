﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Population : MonoBehaviour {

    [Header("Creature"), Range(2, 4)]
    public float flapStrength = 3;
    public GameObject creaturePrefab;

    [Header("UI")]
    public Text creaturesAliveText;
    public Text genText;
    public Text bestFitnessText;
    public Text scoreText;

    internal List<Creature> population = new List<Creature>();

    public int Generation
    { get
        { return _Generation; }
        private set
        {   
            _Generation = value;                     //Everytime we set increments generation
            genText.text = _Generation.ToString();   //We set our generation text
            scoreText.text = "0";                    //Reset score text
            deadCreatureCount = 0;                   //Reset our dead counter
            trackGenerator.ResetPipes();                //Tell gamemanager to start over
        }
    }
    private int _Generation;

    private int populationCount;
    private int deadCreatureCount;
    private int[] neurons = new int[] { 4, 10, 10, 1 };
    private Vector2 creatureStartPos = new Vector2(-4, 0);
    private TrackGenerator trackGenerator;

    /// <summary>
    /// Initialize population
    /// </summary>
    /// <param name="gameManager"></param>
    /// <param name="populationCount"></param>
    public void InitPopulation(TrackGenerator trackGenerator, int populationCount)
    {
        this.trackGenerator = trackGenerator;       //Set variables
        this.populationCount = populationCount;     //Set variables

        Generation++;       //Increments Generation

        for (int i = 0; i < populationCount; i++)   //Initialize creatures to the size of populationCount
        {
            GameObject creature = Instantiate(creaturePrefab, creatureStartPos, Quaternion.identity); //Create creature
            creature.GetComponent<Creature>().Init(this, new NeuralNetwork(neurons), flapStrength);         //Initialize it
            creature.transform.SetParent(transform);                                                        //Set is as our child
            population.Add(creature.GetComponent<Creature>());                                              //Add it to the population
        }
    }

    /// <summary>
    /// When creature dies, it calls this
    /// </summary>
    /// <param name="creature"></param>
    public void OnCreatureDeath(Creature creature)
    {
        deadCreatureCount++;                    //Increment dead counter
        creature.gameObject.SetActive(false);   //Disable the dead
        creaturesAliveText.text = (population.Count - deadCreatureCount).ToString(); //set the creaturesalive text

        if (population.Count == deadCreatureCount)  //If we dont have anyone left anymore we
            NewGeneration();                        //Start the next generation
    }

    /// <summary>
    /// When creature scores, it calls this
    /// </summary>
    /// <param name="creature"></param>
    public void OnCreatureScore(Creature creature)
    {
        if (creature.Score > int.Parse(scoreText.text))     //If the new score is higher than what we have
            scoreText.text = creature.Score.ToString();     //We update the score text to the new value
    }

    /// <summary>
    /// Creates new generation of creatures
    /// </summary>
    private void NewGeneration()
    {
        Generation++;           //Increment generation
       
        List<Creature> newPopulation = new List<Creature>();    //Local variable for new population

        population.Sort(ByFitness);                             //Sort population so that the best individual is in 0:th position
        Creature bestCreature = population[0];                                  //Get the best individual
        bestFitnessText.text = population[0].GetFitness().ToString("0.00");     //Set the best fitness text to the best creatures value

        for (int i = 0; i < populationCount; i++)   //Create the new population
        {
            GameObject creature = Instantiate(creaturePrefab, creatureStartPos, Quaternion.identity); //Instantiate the creature
            creature.GetComponent<Creature>().Init(this, new NeuralNetwork(bestCreature.neuralNetwork), flapStrength);  //Initialize it using the best creature as "Parent"
            creature.GetComponent<Creature>().neuralNetwork.Mutate();       //Mutate the creature so it differs from the best individual
            creature.transform.SetParent(transform);        //Set it as child of this population
            newPopulation.Add(creature.GetComponent<Creature>());   //Add it to the new population
        }

        TerminatePopulation();      //We destroy and clear the old population
        population = newPopulation;     //And we make the new population the current one
    }

    /// <summary>
    /// Destroys and clears the population
    /// </summary>
    private void TerminatePopulation()
    {
        for (int i = 0; i < population.Count; i++)
        {
            Destroy(population[i].gameObject);  //Destroy their objects
        }
        population.Clear(); //And empty the list
    }

    /// <summary>
    /// Used to sort the population by fitness
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private int ByFitness(Creature a, Creature b)
    {
        if (a.GetFitness() > b.GetFitness())      //If a is better than b
            return -1;                              //Return -1
        else if (a.GetFitness() < b.GetFitness()) //Otherwise if b is better than a
            return 1;                               //Return 1
        else                                      //And if they are even
            return 0;                               //Return 0
    }
}
