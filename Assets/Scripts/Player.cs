﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

    private float flapPower;    //Flap strength
    private Rigidbody2D rigidBody;  //my rigidbody
    private PlayerManager playerManager; //playermanager reference

    public void Init(PlayerManager playerManager, float flapPower)
    {
        this.flapPower = flapPower; //set variables
        this.playerManager = playerManager;     //set variables
        rigidBody = GetComponent<Rigidbody2D>();    //Get rigidbody
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0)) //If player presses either space or left mouse button
            Flap(); //we flap
    }

    private void Flap()
    {
        rigidBody.velocity = Vector2.up * flapPower; //apply the force
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacle")     //if we collide with obstacle
            playerManager.OnPlayerDead();               //we tell playermanager that we died
        else if (collision.gameObject.tag == "Clear")   //otherwise if we collide with clear    
            playerManager.OnPlayerScore();              //We cleared a pipe and tell that to the manager
    }
}
