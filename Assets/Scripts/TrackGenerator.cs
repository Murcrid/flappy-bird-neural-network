﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generates the pipes
/// </summary>
public class TrackGenerator : MonoBehaviour {

    [Header("Pipe Pairs")]
    public GameObject pipePairPrefab;
    public Transform pipePairParent;

    [Range(2, 4)]
    public float pipeDelay = 3; //how long it takes to spawn a new pipe
    [Range(1, 3)]
    public float pipeSpeed = 2; //how fast the pipes move
    [Range(4.0f, 5.5f)]
    public float startingPipeDistance = 4.5f;  //in the beginning, how far apart the two pipes are

    private float runningPipeDelay; //How long it take before next pipe is created
    private float currentPipeDistance;  //The distance between pipes

    private List<GameObject> pipePairs = new List<GameObject>(); //List of pipes

    public void Init()
    {
        currentPipeDistance = startingPipeDistance; //In the start we set the user given values
    }

    private void Update()
    {
        if (runningPipeDelay <= 0)                                                      //if it is time to spawn new pipe
        {
            GeneratePipePair();                         //we create the new pipe
            runningPipeDelay = pipeDelay;               //and reset the timer

            if (pipePairs[0].transform.position.x <= -11) //Also if the left most pipe is at -11x or less, 
            {
                Destroy(pipePairs[0]);                  //we destroy it
                pipePairs.Remove(pipePairs[0]);         //and we remove it
            }
        }
        else                                                                            //otherwise 
            runningPipeDelay -= Time.deltaTime;                                         //reduce the time 
    }

    /// <summary>
    /// Creates new pair of pipes
    /// </summary>
    public void GeneratePipePair()
    {
        currentPipeDistance -= 0.1f;    //Make pipes closer to eachother than in previous pipes

        GameObject pipePair = Instantiate(pipePairPrefab, pipePairParent);      //Create new pipepair
        pipePair.GetComponent<PipePair>().Init(currentPipeDistance, pipeSpeed);   //And initialize it
        pipePairs.Add(pipePair); //And add it to the list     
    }

    /// <summary>
    /// Called by population when new generation is born
    /// </summary>
    public void ResetPipes()
    {
        for (int i = 0; i < pipePairs.Count; i++)
        {
            Destroy(pipePairs[i]);  //Destroy the pipes
        }
        pipePairs.Clear(); //Clear the list

        currentPipeDistance = startingPipeDistance; //Reset the distance between the pipes
        runningPipeDelay = 0;   //And set the running pipe delay to 0 so the new pipe gets spawned immediately
    }
}
