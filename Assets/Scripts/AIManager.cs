﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIManager : MonoBehaviour {

    [Header("Track Generator")]
    public TrackGenerator trackGenerator;

    [Header("Population")]
    public Population population;   //The population
    [Range(20, 100), Space(5)]
    public int populationCount = 20; //How many cretures in a family

    private void Start()
    {
        population.InitPopulation(trackGenerator, populationCount);   //And initialize our family
    }
}
