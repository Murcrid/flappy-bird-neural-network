﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Creature : MonoBehaviour {

    internal float Score;       //Stores the count of pipes passed
    internal NeuralNetwork neuralNetwork;   //Stores the neural network of this Creature

    private Population family;  //Our family
    private Rigidbody2D rb; //Our rigidbody
    private GameObject closestPipe; //The pipe that is closest to us

    private bool isDead;    //Are we dead
    private float pipeDistanceScore;    //How close to the next pipe we got
    private float flapPower;    //The multiplier used to give the velocity when flapping

    /// <summary>
    /// Initializes the creature
    /// </summary>
    /// <param name="family"></param>
    /// <param name="network"></param>
    /// <param name="power"></param>
    public void Init(Population family, NeuralNetwork network, float power)
    {
        this.flapPower = power;     //Setting the variables
        this.family = family;       //Setting the variables
        neuralNetwork = network;    //Setting the variables

        rb = GetComponent<Rigidbody2D>();   //Getting the attached Rigidbody component
    }

    /// <summary>
    /// Returns this creatures fitness value
    /// </summary>
    /// <returns></returns>
    public float GetFitness()
    {
        return Score + pipeDistanceScore;
    }

    /// <summary>
    /// Done every frame
    /// </summary>
    private void Update()
    {
        if (!isDead)                    //If we are not dead,
        {
            if (closestPipe == null)    //We check if closestPipe is has not been nulled
                FindNextPipe();         //If it has, we get a new closest pipe                                        
            else                        //otherwise
            {               
                float[] observations = GetObservations();                   //We collect observations
                float[] output = neuralNetwork.FeedForward(observations);   //We calculate what the neural network thinks we should do with given observations
                if (output[0] > 0.5f)                                       //if our output is higher than trigger value
                    Flap();                                                 //We need to go higher, aka flap.
            }
        }
    }

    private void FindNextPipe()
    {
        float distToClosestPipe = 0;            //Local variable for distance to closest pipe
        GameObject[] pipes = GameObject.FindGameObjectsWithTag("PipePair");     //Gather all pipes
        foreach (var item in pipes)                                             //And for each of the pipes we
        {
            float distToPipe = Vector2.Distance(item.transform.position, transform.position);   //Calculate the distance to it
            if ((closestPipe == null || distToPipe < distToClosestPipe) && item.transform.position.x > transform.position.x)    //And if we dont have one or if the distance is more than the old closes one
            {                                                                                                                   //AND the new pipe is in front of us
                distToClosestPipe = distToPipe;     //We set the distance to the closest to be the new distance
                closestPipe = item;                 //And we set the closest pipe to be the new pipe    
            }
        }
    }

    /// <summary>
    /// Returns the array of the observations that the creature collects
    /// </summary>
    /// <returns></returns>
    private float[] GetObservations()
    {
        float[] observations = new float[neuralNetwork.inputSize];        //Local variable to store the observations

        observations[0] = (closestPipe.transform.GetChild(0).position.x - transform.position.x) - 1;        //First we get the horizontal distance to the next pipe
        observations[1] = Mathf.Abs(closestPipe.transform.GetChild(0).position.y - transform.position.y) + 2.5f;    //Then we get the vertical distance to the upper pipe
        observations[2] = Mathf.Abs(closestPipe.transform.GetChild(1).position.y - transform.position.y) - 2.5f;    //And vertical distance to the lower pipe                            

        return observations;    //Return the observations we collected
    }

    /// <summary>
    /// Used to go higher
    /// </summary>
    private void Flap()
    {
        rb.velocity = Vector2.up * flapPower;       //Apply the velocity
    }

    /// <summary>
    /// When pipe is reached
    /// </summary>
    private void PipeClear()
    {
        if (!isDead)    //If we are not dead
        {
            Score++;    //Increment score
            closestPipe = null;     //Set closest pipe to null, since we reached the previous one
            family.OnCreatureScore(this);   //let the family know that we scored... Kappa
        }
    }

    /// <summary>
    /// When we hit wall or drop down or go too high
    /// </summary>
    private void Terminate()
    {
        if (!isDead) //If we are not dead yet
        {
            isDead = true;  //Now we are
            if (closestPipe != null)    //If the closest pipe exist
            {
                float normalizedDistanceToNextPipe = Vector2.Distance(transform.position.normalized, closestPipe.transform.position.normalized);    //Calculate the normalized distance to the next pipe
                pipeDistanceScore = 1 / normalizedDistanceToNextPipe;   //Get the score out of the normalized distance
            }
            family.OnCreatureDeath(this);   //Let the family know we died
        }
    }

    /// <summary>
    /// Collision detections
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle")    //If we collide with an obstacle
            Terminate();                    //We terminate
        else if (collision.tag == "Clear")  //otherwise if we collide with Clear
            PipeClear();                    //we passed a pipe
    }
}
